Rails.application.routes.draw do

  root 'index#index'

  get 'index/index'

  resources :recipe, :only => [:index, :create, :destroy]

end
