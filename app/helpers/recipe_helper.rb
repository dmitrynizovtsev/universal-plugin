module RecipeHelper

  def plugin_tpls
    partials = []
    Dir[Rails.root.join('app/views/recipe/tpls/*.*')].each { |f| partials << 'recipe/tpls/' + File.basename(f).split('/').last.split('.').first[1..-1] }
    partials
  end

end
