Recipe.Views.RehabTriggers = Backbone.View.extend({
    template: "script#rehab-triggers-tpl",

    render: function () {
        var template = _.template($(this.template).html());
        this.$el.html(template({}));
        return this;
    }
});