Recipe.Views.RxcartTriggers = Backbone.View.extend({
    template: "script#rxcart-triggers-tpl",

    render: function () {
        var template = _.template($(this.template).html());
        this.$el.html(template({}));
        return this;
    }
});