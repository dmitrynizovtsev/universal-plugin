Recipe.Views.VsrTriggers = Backbone.View.extend({
    template: "script#vsr-triggers-tpl",

    render: function () {
        var template = _.template($(this.template).html());
        this.$el.html(template({}));
        return this;
    }
});