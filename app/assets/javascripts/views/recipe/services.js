Recipe.Views.Services = Backbone.View.extend({
    template: "script#services-tpl",
    el: '<ul class="up_services"></ul>',

    render: function () {
        var template = _.template($(this.template).html());
        this.$el.html(template({}));
        return this;
    }
});