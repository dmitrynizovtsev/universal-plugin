window.Recipe = {
    Models : {},
    Collections : {},
    Views : {},
    Routers : {},
    Helpers : {},
    init : function () {
        new Recipe.Routers.Index();
        Backbone.history.start();
    }
};

$('.recipe.index').ready(function () {
    Recipe.init();
});
