Recipe.Routers.Index = Backbone.Router.extend({

    routes: {
        "!/rehab/new_sales" : "rehab_new_sales",
        "!/rehab" : "rehab",
        "!/rxcart" : "rxcart",
        "!/vsr" : "vsr",
        "*path": "index"
    },

    index : function () {
        console.log("index");
        this.services = new Recipe.Views.Services();
        $('.up_main').html(this.services.render().el);
    },

    rehab : function () {
        console.log("rehab");
        var triggers = new Recipe.Views.RehabTriggers();
        $('.up_main').html(triggers.render().el);
    },

    rxcart : function () {
        console.log("rxcart");
        var triggers = new Recipe.Views.RxcartTriggers();
        $('.up_main').html(triggers.render().el);
    },

    vsr : function () {
        console.log("vsr");
        var triggers = new Recipe.Views.VsrTriggers();
        $('.up_main').html(triggers.render().el);
    }
});